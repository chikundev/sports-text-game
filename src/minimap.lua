-- This will draw the minimap

function minimapDraw()
    for k, v in ipairs(currentState.noun) do
        if v.x then
            if v.symbol then
                for i = 1, v.width do
                    for o = 1, v.height do
                        love.graphics.setColor(255, 255, 255)
                        love.graphics.print(v.symbol, ((v.x + i - 1) * 10) + 500, (v.y + o - 1) * 10)
                    end
                end
            end
        end
    end
    local f = currentState
    love.graphics.setColor(255, 255, 255)
    love.graphics.rectangle("line", mainScreen.width + ((f.x * 10) + 5), (f.y * 10) + 5, f.width * 10, f.height * 10 )
end
