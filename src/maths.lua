--Integral math functions

function math.round(value)
    return(math.floor(value + 0.5))
end

function math.sign(value)
    returnValue = 0
    if (value > 0) then
        returnValue = 1
    end
    if (value < 0) then
        returnValue = -1
    end
    return(returnValue)
end

function math.checkCollision(obj1, obj2)
    returnValue = false

    if (obj1.x + obj1.width > obj2.x and
        obj1.x < obj2.x + obj2.width and
        obj1.y + obj1.height > obj2.y and
        obj1.y < obj2.y + obj2.height) then
        returnValue = true
    end
    return(returnValue)
end
