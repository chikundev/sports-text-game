function love.textinput(t)
    text = text .. t
    cursorReset()
end

function textDraw()
    love.graphics.setColor(255, 255, 255)
    drawText = ">" .. text
    if cursor then
        drawText = drawText .. "_"
    end
    love.graphics.printf(drawText, 10, love.graphics.getHeight() - 20, mainScreen.width - 20)
end

function textUpdate(dt)
    cursorTimer = cursorTimer + dt
    if cursorTimer >= 0.5 then
        cursorTimer = cursorTimer - 0.5
        if cursor then
            cursor = false
        else
            cursor = true
        end
    end
end

function cursorReset()
    cursor = true
    cursorTimer = 0
end
