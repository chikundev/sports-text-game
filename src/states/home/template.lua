-- Home template state

local newState = {
    noun = {
        list = {
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
                },
            work = {
                past = "tense of verb"
            }
        },
        of = {
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            }
        },
        nouns = {
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            }
        }
    }
}

return newState
