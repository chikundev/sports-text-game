local newState = {
    x      = 0,
    y      = 0,
    width  = 11,
    height = 6,
    noun = {
        {
            name   = "bed",
            x      = 9,
            y      = 1,
            width  = 2,
            height = 1,
            symbol = "B",
            sleep = {
                past = "slept in"
            },
            lay = {
                past = "lied on"
                },
            lie = {
                past = "lied on"
            },
            make = {
                past = "made the"
            }
        },
        {
            name   = "closet",
            x      = 7,
            y      = 1,
            width  = 2,
            height = 1,
            symbol = "C",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            }
        },
        {
            name   = "table",
            x      = 10,
            y      = 2,
            width  = 1,
            height = 1,
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name   = "lamp",
            x      = 10,
            y      = 2,
            width  = 1,
            height = 1,
            symbol = "L",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name   = "lego",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name   = "door",
            x      = 1,
            y      = 6,
            width  = 1,
            height = 1,
            symbol = "D",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name   = "desk",
            x      = 10,
            y      = 4,
            width  = 1,
            height = 2,
            symbol = "d",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name   = "chair",
            x      = 9,
            y      = 5,
            width  = 1,
            height = 1,
            symbol = "c",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name   = "powerplug",
            adjective = "left",
            x      = 10,
            y      = 3,
            width  = 1,
            height = 1,
            symbol = "P",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name = "powerplug",
            adjective = "right",
            x      = 10,
            y      = 3,
            width  = 1,
            height = 1,
            symbol = "P",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name = "switch",
            adjective = "left",
            x      = 10,
            y      = 3,
            width  = 1,
            height = 1,
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name = "switch",
            adjective = "right",
            x      = 10,
            y      = 3,
            width  = 1,
            height = 1,
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name = "light",
            x      = 6,
            y      = 3,
            width  = 1,
            height = 1,
            symbol = "L",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        },
        {
            name = "switch",
            adjective = "light",
            x      = 2,
            y      = 5,
            width  = 1,
            height = 1,
            symbol = "s",
            verbs = {
                past = "tense of verb"
            },
            that = {
                past = "tense of verb"
            },
            work = {
                past = "tense of verb"
            },
        }
    }
}

return newState
