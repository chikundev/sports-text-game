-- load all scripts for startup

require "src/maths"             -- Add extra math functions
require "src/loadGFX"           -- Load all images
require "src/loadSFX"           -- Load all sounds
require "src/input"             -- Inputting text into game
require "src/interpretString"   -- String interpretation
require "src/loadStates"        -- Loading all states on startup
require "src/minimap"           -- Load up minimap and its relevant functions
