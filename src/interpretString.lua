previousInput = { }
verb = {
    open = {
        past = "opened"
        },
    close = {
        past = "closed"
        },
    look = {
        past = "looked"
        },
    eat = {
        past = "ate"
        },
    move = {
        past = "moved"
        },
    sit = {
        past = "sat"
        },
    stand = {
        past = "stood"
        },
    fart = {
        past = "farted"
        }
    }
noun = {
    car = { },
    door = { },
    ocean = { },
    noodle = { },
    window = { },
    chair = { },
    trapdoor = { },
    face = { }
    }
adjective = { }
adverb = { }

function interpretString(t)
    --set previous inputs
    previousInput[#previousInput + 1] = t
    t = t:lower()
    local split = { }
    for i in t:gmatch("%S+") do
        table.insert(split, i)
    end
    foundObject = false
    for k, v in ipairs(currentState.noun) do

        print("a")
        if v.name == split[2] then
            print("b")
            foundObject = true
            if v[split[1]] then
                print("c")
                previousInput[#previousInput + 1] = "I just " .. v[split[1]].past .. " " .. split[2] .. "."

            else

                previousInput[#previousInput + 1] = "Sorry, I don't know how to " .. split[1] .. " a " .. split[2] .. "."

            end
        end
    end
    if not foundObject then
        previousInput[#previousInput + 1] = "Sorry, but " .. split[2] .. " does not seem to exist in this room."
    end
    --split string up into parts
    --check if parts are words
    --check if words are valid
end

function inputDraw()
    --display previous inputs
    for k, v in ipairs(previousInput) do
        love.graphics.printf(v, 10, 10 + (16 * (k - 1)), mainScreen.width - 20)
    end
end
