
-- on game start
function love.load()

    -- load all scripts on startup
    require "src/onStartup"

    -- Set current state
    currentState = states.home.template

    text = ""
    cursorReset()

    -- change background colour
    love.graphics.setBackgroundColor(0, 0, 0)

    currentState = states.home.bedroom3

end


-- on game step
function love.update(dt)

    textUpdate(dt)

end


-- draw
function love.draw()

    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle("fill", mainScreen.width, 0, 140, 360)
    love.graphics.setColor(255, 255, 255)
    love.graphics.rectangle("line", mainScreen.width,   0, 140, 140)
    love.graphics.rectangle("line", mainScreen.width, 140, 140, 260)

    textDraw()
    inputDraw()
    minimapDraw()

end


function love.keypressed(key)
    if (key == "escape") then
        love.event.quit()
    elseif (key == "backspace") then
        text = text:sub(1, text:len() - 1)
        cursorReset()
    elseif (key == "return") then
        interpretString(text)
        text = ""
        cursorReset()
    end
end

--Info for game input screen
mainScreen = {
    x = 0,
    y = 0,
    height = love.graphics.getHeight(),
    width = love.graphics.getWidth() - 140
    }
